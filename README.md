# Internship Interview Questions
*Please include explanation along with your answers.*

1. Please describe yourself using JSON (include your internship start/end date).
```json
{
   "name":"Ng Zhen Hao",
   "dob":"24-1-1997",
   "place_of_birth":"Ipoh",
   "status":"Single",
   "email":"yizake1820@gmail.com",
   "phone_number":"011-24184791",
   "education":{
      "PMR":"Passed",
      "SPM":"Passed",
      "STPM":"Passed",
      "Degree":"Ongoing"
   },
   "intern_date":{
      "start":"19 Oct 2020",
      "end":"4 Apr 2021",
      "duration":"6 Months"
   },
   "programming_languages_learnt":[
      "Android Development",
      "Java",
      "Kotlin",
      ".NET C#",
      "HTML/CSS/JS",
      "PHP",
      "Python"
   ],
   "is_looking_for_job":true,
   "will_contribute_to_company":true
}
```

2. Tell us about a newer (less than five years old) web technology you like and why?
>>>
The newest technology that fancies me recently is Progressive Web Apps (PWA). PWA combines the speed of native apps and cross-compatibility of browsers. If implemented correctly, end-users are more likely to give PWA a try because they do not have to explicitly install it from App Store.
>>>

3. In Java, the maximum size of an Array needs to be set upon initialization. Supposedly, we want something like an Array that is dynamic, such that we can add more items to it over time. Suggest how we can accomplish that (other than using ArrayList)?
>>>
Use `java.util.LinkedList`

Or reallocate an array with different size and copy the content of old array into the new array using the `arrayCopy()` method
>>>

4. Explain this block of code in Big-O notation.
    ```
    void sampleCode(int arr[], int size)
    {
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                printf("%d = %d\n", arr[i], arr[j]);
            }
         }
    }
    ```
>>>
The complexity of the code block above is O(n^2). It is because the everytime the outer loop runs once, the inner loop has to run for N times. 
>>>

5. One of our developers built a page to display market information for over 500 cryptocurrencies in a single page. After several days, our support team started receiving complaints from our users that they are not able to view the website and it takes too long to load. What do you think causes this issue and suggest at least 3 ideas to improve the situation. 
>>>
Most likely the website performance is bottlenecked by the process to load huge amount of information in short time (eg. 500 cryptocurrencies is a lot for browsers to render). First thing to come in mind is to paginate the results if it is not yet done. Pagination reduces load and thus increases the rendering speed for browsers. 

Next, I would try replacing the external JS libraries such as JQuery, DataTables with the ones from CDN. This is because users are very likely to have the cache of the libraries obtained from other sites.

Last, I would check the performance of the database queries. It is possible that the query takes a very long time to run. Or a deadlock has occured.
>>>
6. In Javascript, What is a "closure"? How does JS Closure works?
>>>
A closure is when the inner function scope is able to access the variables inside the outer function scope. JS closures work by maintaining references to the outer instance
>>>

7. In Javascript, what is the difference between var, let, and const. When should I use them?
>>>
`var` is function scoped or globally scoped depending on where it is declared. `let` and `const` are block scoped

`const` is used for variables that will not be reassigned such as `const PI = 3.142`

`let` is used for variables that is used only in its block, such as counter in a loop, or in closures to bind new values rather than old values.

`var` is for general usage.
>>>

---
# Simple Coding Assessment

Build a URL shortener service (refers to https://bitly.com)

requirement:
1. build a method/function where it takes in an URL (example www.coingecko.com), and the output is a shortened URL
2. the length of the shortened URL must be unique.
3. build a method/function where it takes in the shortened URL in #1, and the output is the full URL.
4. you must be able to demo your code to us.
5. **(bonus point)** use Ruby On Rails and/or ReactJS as the language/framework.
6. **(bonus point)** host it on a website.

---
# Submission instruction

1. fork this repo
2. creates a Merge Request against the master branch (after completion)
3. grant repo access (collaborators) to jack@coingecko.com and tmlee@coingecko.com
